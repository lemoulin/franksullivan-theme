<?php
use FrankSullivan\Assets;

// get header image
$header_image = get_field('faqs_header_image', 'options');
$faqs_intro_text = get_field('faqs_intro_text_' . pll_current_language(), 'options');

?>

<section id="faqs" <?php post_class(); ?>>

  <header class="section--compact-header">
    <hgroup class="container-narrow">
      <h1 class="title is-1"><?php pll_e('FAQ') ?></h1>
    </hgroup>
    <?php if ($header_image): ?>
    <figure class="loading-content">
      <div class="rellax" data-rellax-speed="-4">
        <?php echo get_responsive_image($header_image, 'largest', 'bg') ?>
      </div>
    </figure>
    <?php endif; ?>
  </header>

  <div class="container-narrow faqs--list">

    <header class="page--intro-blurb">
      <?= $faqs_intro_text ?>
    </header>

    <?php while ( have_posts() ) : the_post(); ?>
    <article class="faq--item" data-accordion>
      <header>
        <h3 class="title is-5">
          <a href="#" class="accordion--item--toggle">
            <?php the_title() ?>
          </a>
          <a href="#" class="accordion--item--toggle faq--item--close pull-right"><i class="ion-ios-close-empty"></i></a>
        </h3>
      </header>
      <footer class="faq--item--content accordion--item--content">
        <?php the_content() ?>
      </footer>
    </article>
    <?php endwhile; ?>

  </div>

</section>

<?php get_template_part('templates/partials/newsletter') ?>

<?php get_template_part('templates/posts/recent-posts-square') ?>
