<?php

use FrankSullivan\Setup;
use FrankSullivan\Wrapper;
use FrankSullivan\Assets;

?>

<!doctype html>
<html <?php language_attributes(); ?> class="page--loading site--loading">

  <?php get_template_part('templates/head'); ?>

  <body id="document" <?php body_class(); ?>>

    <?php // page transition div, inline CSS prevent defered visual bug ?>
    <div class="page-loading--transition" style="background-color: #fff; position: fixed; top: 0; left: 0; right: 0; bottom: 0; width: 100vw; height: 100vh; z-index: 10000;">
      <figure style="display: none;">
        <object class="svg-animation" type="image/svg+xml" data="<?= Assets\get_file_path('svg/fs-logo.svg') ?>"></object>
      </figure>
    </div>

    <main id="barba-wrapper" class="page-loading--container">

      <div class="barba-container">

        <!-- top -->
        <a id="top"></a>

        <!--[if IE]>
          <div class="alert alert-warning">
            <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
          </div>
        <![endif]-->

        <?php if (is_home()): ?>
          <?php // carousel on homepage only ?>
          <?php get_template_part('templates/elements/home-carousel'); ?>
        <?php endif; ?>

        <?php
          do_action('get_header');
          get_template_part('templates/header');
        ?>

        <div class="wrap" role="document">

            <main id="main" class="main">
              <?php include Wrapper\template_path(); ?>
            </main><!-- /.main -->

        </div><!-- /.wrap -->

        <?php get_template_part('templates/partials/modal-follow-us'); ?>

        <?php // site footer ?>
        <?php get_template_part('templates/footer'); ?>

      <?php
        // WP footer
        do_action('get_footer');
        wp_footer();
      ?>

      </div>

    </main>

  </body>
</html>
