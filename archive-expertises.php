<?php
use FrankSullivan\Assets;
use FrankSullivan\PostQueries;

// get fields
$header_image = get_field('expertises_header_image', 'options');

?>

<section id="expertises" data-controller="Expertises">

  <header class="section--compact-header">
    <footer class="container">
      <h1 class="title is-1">
        <span class="title-line">
          <span class="title-line--inner"><?php pll_e('Expertises') ?></span>
        </span>
      </h1>
    </footer>
    <?php if ($header_image): ?>
    <figure class="loading-content">
      <div class="rellax" data-rellax-speed="-4">
        <?php echo get_responsive_image($header_image, 'largest', 'bg') ?>
      </div>
    </figure>
    <?php endif; ?>
  </header>

  <div class="expertises--list container">
    <div class="columns is-multiline">
      <?php $i = 1; while ( have_posts() ) : the_post(); ?>
        <?php get_template_part('templates/expertises/expertise-item') ?>
      <?php $i++; endwhile; ?>
    </div>
  </div>

  <?php get_template_part('templates/posts/recent-posts-square') ?>

</section>
