
<?php

use FrankSullivan\PostQueries;

// queries
$promotions        = PostQueries\Query('promotions', -1);
$products_on_homepage = PostQueries\Query('products_on_homepage', -1);
$recent_posts      = PostQueries\Query('get_posts', 1);

// fields
$background_image = get_field('home_header_image');
$background_video = get_field('home_background_video');

?>

<div id="home" class="home" data-controller="Home">

  <header class="section--intro-header">

    <div class="container">

        <hgroup>

          <hgroup class="section--intro-header--intro-blurb">
            <?php the_content() ?>
          </hgroup>

          <div class="is-hidden-mobile columns is-gapless" data-aos="fade" data-aos-duration="1000" data-aos-delay="2000">
            <hgroup class="column is-9">
              <?php the_field('home_header_in_store') ?>
            </hgroup>
          </div>

      </hgroup>

    </div>
    <!-- /container -->

    <?php if ($background_image): ?>
    <figure class="section--intro-header--background-image <?php if ( !isMobile() ): ?>rellax<?php endif; ?>" data-rellax-speed="-5">
      <?php if ($background_video): ?>
      <?php echo get_background_video($background_video) ?>
      <?php endif; ?>
      <?php echo get_responsive_image($background_image, 'largest', 'bg', false) ?>
    </figure>
    <?php endif; ?>

  </header>

  <!-- Intro visible only on tablet and mobile -->
  <section class="home--header-in-store-intro container is-hidden-desktop">
      <?php the_field('home_header_in_store') ?>
  </section>

  <!-- promotions -->
  <section class="home--promotions container <?php if ( !isMobile() ): ?>rellax<?php endif; ?>" data-rellax-speed="0.50">
    <div class="columns is-multiline is-slider-mobile">
      <?php $i=0; while ($promotions->have_posts()) : $promotions->the_post(); ?>
        <aside class="column <?php the_field('promotion_column') ?> is-slide">
          <div <?php if ( !isMobile() ): ?>data-aos="fade-up" data-aos-duration="1250" data-aos-delay="<?= object_transition_stagger_speed($i, 100, 500, 3) ?>"<?php endif; ?>>
            <?php get_template_part('templates/promotions/promotion-preview-pane'); ?>
          </div>
        </aside>
      <?php $i++; endwhile; wp_reset_postdata(); ?>
    </div>
  </section>

  <!-- experience -->
  <section class="home--experience">

    <div class="container">

      <section class="columns is-multiline is-left-padded is-right-padded">
        <aside class="column is-6 is-with-vertical-spacer">
          <?php the_field('home_experience_intro') ?>
        </aside>
        <aside class="column is-5 is-with-vertical-spacer">
          <?php the_field('home_experience_text') ?>
        </aside>
        <figure class="column is-6 box--square" <?php if ( !isMobile() ): ?>data-aos="fade-right" data-aos-duration="1250" data-aos-offset="500"<?php endif; ?>>
          <div class="box--content">
            <?php echo get_responsive_image(get_field('home_experience_photo_1'), 'largest', 'bg') ?>
          </div>
        </figure>
        <figure class="column is-6 box--square" <?php if ( !isMobile() ): ?>data-aos="fade-left" data-aos-duration="1250" data-aos-offset="500"<?php endif; ?>>
          <div class="box--content">
            <?php echo get_responsive_image(get_field('home_experience_photo_2'), 'largest', 'bg') ?>
          </div>
        </figure>
      </section>

    </div>
    <!-- /.container -->

  </section>
  <!-- /experience -->

  <!-- brands -->
  <?php if (have_rows('home_brands_logos')): ?>
  <section class="home--brands is-with-vertical-spacer hide">

    <div class="container">

      <header class="columns is-multiline">
        <hgroup class="column is-7 is-offset-1">
          <?php the_field('home_brands_intro') ?>
        </hgroup>

        <hgroup class="columns column is-11 is-offset-1 is-multiline is-mobile">
          <?php while ( have_rows('home_brands_logos') ) : the_row(); ?>
            <figure class="home--brands--logo column is-2-desktop is-4-tablet is-6-mobile">
              <img src="<?php the_sub_field('home_brands_logo_image') ?>" alt="logo">
            </figure>
          <?php endwhile; ?>
        </hgroup>
      </header>

    </div>

  </section>
  <?php endif; ?>

  <!-- home footer -->
  <section class="home--footer">

    <header class="container">

      <hgroup class="columns is-half-left-padded">

        <aside class="column is-6">

          <div class="home--footer--text">
            <?php the_field('home_footer_text') ?>
          </div>

          <div class="home--foter--image--mobile is-hidden-desktop">
            <?php echo get_responsive_image(get_field('home_footer_image'), 'large') ?>
          </div>

          <footer class="posts--list-recent">
            <h6 class="posts--list-recent--label title is-6"><?php pll_e("From our design crew") ?></h6>
            <?php while ($recent_posts->have_posts()) : $recent_posts->the_post(); ?>
              <aside data-aos="fade-up" data-aos-duration="1250" data-aos-offset="75">
                <?php get_template_part('templates/posts/post-preview-square'); ?>
              </aside>
            <?php endwhile; wp_reset_postdata(); ?>
          </footer>

        </aside>

        <aside class="column is-6 is-hidden-touch">
          <?php echo get_responsive_image(get_field('home_footer_image'), 'large') ?>
        </aside>

      </hgroup>

    </header>

    <footer class="home--footer--contact container">

      <div class="columns">

        <aside class="column is-3" data-aos="fade-right" data-aos-duration="1250" data-aos-offset="150">
          <h4 class="title is-2"><?php pll_e("Contact") ?></h4>
        </aside>

        <aside class="column is-7" data-aos="fade-right" data-aos-duration="1250" data-aos-delay="250" data-aos-offset="150">
          <?php the_field('home_footer_contact') ?>
        </aside>

        <aside class="column is-narrow is-pull-right" data-aos="fade-right" data-aos-duration="1250" data-aos-delay="500" data-aos-offset="150">
          <?php the_field('footer_address', 'options') ?>
        </aside>

      </div>

    </footer>

    <!-- black bar on top -->
    <div class="home--footer--bg"></div>

  </section>
  <!-- /.home footer -->

</div>

<?php get_template_part('templates/partials/newsletter') ?>
