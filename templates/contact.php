<?php

// get fields
$contact_address = get_field('contact_address_' . pll_current_language(), 'options');
$contact_hours = get_field('contact_hours_' . pll_current_language(), 'options');
$mailchimp_form = get_field('mailchimp_form_' . pll_current_language(), 'options');
$newsletter_intro = get_field('newsletter_intro_' . pll_current_language(), 'options');
$contact_map_coordinates = get_field('contact_map_coordinates', 'options');
$facebook_url = get_field('facebook_url', 'options');
$twitter_url = get_field('twitter_url', 'options');
$instagram_url = get_field('instagram_url', 'options');

?>

<article id="<?= $post->post_name ?>" <?php post_class('contact-page'); ?> data-controller="Contact">

  <header class="container-fluid">

    <div class="columns is-multiline">

      <!-- first columns with page content -->
      <aside class="column is-3-desktop is-6-tablet contact-page--address">
        <h3><?php the_title() ?></h3>
        <?php the_content() ?>
      </aside>

      <!-- Opening hours -->
      <aside class="column is-4-desktop is-6-tablet contact-page--phone">
        <?= $contact_address ?>
        <div class="text-overflow--tablet">
          <?= $contact_hours ?>
        </div>
      </aside>

      <!-- Newsletter -->
      <aside class="column is-3-desktop is-6-tablet contact-page--newsletter">
        <h3><?php pll_e("Get updates") ?></h3>
        <?= $newsletter_intro ?>
        <?= $mailchimp_form ?>
      </aside>

      <!-- Opening hours -->
      <aside class="column is-2-desktop is-6-tablet contact-page--social --is-pull-right">
        <h3><?php pll_e("Follow us") ?></h3>
        <ul class="social-links">
          <?php if ($facebook_url): ?>
          <li><a href="<?= $facebook_url ?>" target="_blank"><i class="ion-social-facebook"></i> Facebook</a></li>
          <?php endif; ?>
          <?php if ($twitter_url): ?>
          <li><a href="<?= $twitter_url ?>" target="_blank"><i class="ion-social-twitter"></i> Twitter</a></li>
          <?php endif; ?>
          <?php if ($instagram_url): ?>
          <li><a href="<?= $instagram_url ?>" target="_blank"><i class="ion-social-instagram-outline"></i> Instagram</a></li>
          <?php endif; ?>
        </ul>
      </aside>

    </div>

  </header>

  <!-- bottom with map -->
  <footer class="contact-page--map container-fluid">
    <nav>
      <a href="#" data-toggle-visibile=".contact-page--map--gmap" class="btn--outlined"><i class="ion-compass"></i> <?php pll_e('Show Map') ?></a>
    </nav>
    <div class="contact-page--map--container">
      <div class="box--content">
        <div class="contact-page--map--gmap" data-lat="<?= $contact_map_coordinates['lat'] ?>" data-lng="<?= $contact_map_coordinates['lng'] ?>"></div>
      </div>
      <?php echo get_responsive_image(null, 'largest', 'bg', false) ?>
    </div>
  </footer>


</article>
