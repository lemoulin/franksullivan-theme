<?php

// get fields
$contact_hours = get_field('contact_hours_' . pll_current_language(), 'options');
$phone_number = get_field('phone_number', 'options');

// social
$facebook_url = get_field('facebook_url', 'options');
$twitter_url = get_field('twitter_url', 'options');
$instagram_url = get_field('instagram_url', 'options');

?>

<header class="site-header">

  <hgroup class="site-header--opening-hours">
    <div class="container-fluid">
      <nav class="opening-hours--nav-bar level">
        <a href="#" class="opening-hours--toggle level-item"><span><?php pll_e('We are open') ?></span> <i class="ion-ios-arrow-down"></i></a>
        <a href="tel:<?= str_replace(' ', '', $phone_number) ?>" class="telephone level-right is-hidden-mobile"><?= $phone_number ?></a>
      </nav>
      <footer class="opening-hours--details level is-desktop">
        <div class="level-item">
          <aside class="opening-hours--details--content">
            <?= $contact_hours ?>
          </aside>
        </div>
      </footer>
      <hr class="is-hidden-desktop">
    </div>
  </hgroup>

  <hgroup class="site-header--nav banner">

    <div class="container-fluid">
      <div class="level is-mobile">

        <aside class="level-left">
          <a class="brand" href="<?= esc_url(home_url('/')); ?>"><?php bloginfo('name'); ?></a>
        </aside>

        <nav class="level-right nav-primary is-hidden-touch">
          <?php
          if (has_nav_menu('primary_navigation')) :
            wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'is-inlined']);
          endif;
          ?>
        </nav>

        <aside class="level-right is-hidden-desktop">
          <a href="#" class="toggle-nav-mobile">
            <div class="inner">
              <span class="btn--text"><?php pll_e("Menu") ?></span>
              <span class="btn--close is-inverted"></span>
            </div>
          </a>
        </aside>

      </div>
      <!-- /.level -->

      <!-- mobile nav  -->
      <nav class="nav-primary--mobile is-hidden-desktop">
        <div class="nav-primary--mobile--content">
          <?php
          if (has_nav_menu('primary_navigation')) :
            wp_nav_menu(['theme_location' => 'primary_navigation']);
          endif;
          ?>
          <hr>
          <ul class="social-links is-inlined">
            <?php if ($facebook_url): ?>
            <li><a href="<?= $facebook_url ?>" target="_blank"><i class="ion-social-facebook"></i> Facebook</a></li>
            <?php endif; ?>
            <?php if ($twitter_url): ?>
            <li><a href="<?= $twitter_url ?>" target="_blank"><i class="ion-social-twitter"></i> Twitter</a></li>
            <?php endif; ?>
            <?php if ($instagram_url): ?>
            <li><a href="<?= $instagram_url ?>" target="_blank"><i class="ion-social-instagram-outline"></i> Instagram</a></li>
            <?php endif; ?>
          </ul>
        </div>
      </nav>
      <!-- /mobile nav  -->

    </div>
  </hgroup>

</header>
