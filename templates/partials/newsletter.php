<?php

// get fields
$mailchimp_form = get_field('mailchimp_form_' . pll_current_language(), 'options');
$newsletter_intro = get_field('newsletter_intro_' . pll_current_language(), 'options');

?>

<footer class="newsletter--footer-bar container">
  <div class="columns">
    <aside class="column is-3 is-offset-1" data-aos="fade" data-aos-duration="1250">
      <?= $newsletter_intro ?>
    </aside>
    <aside class="column is-7 is-offset-1" data-aos="fade" data-aos-duration="1250" data-aos-delay="350">
      <?php if ($mailchimp_form): ?>
      <?= $mailchimp_form ?>
      <?php endif; ?>
    </aside>
  </div>
</footer>
