<nav class="sharer">
  <ul>
    <li><a href="<?php the_permalink() ?>" class="btn-share" data-share-facebook title="<?php wp_get_document_title() ?>"><i></i> <?php pll_e("Share") ?></a></li>
    <!-- <li><a href="<?php the_permalink() ?>" class="btn-share" data-share-twitter title="<?php wp_get_document_title() ?>"><i></i> <?php pll_e("Share") ?></a></li> -->
  </ul>
</nav>
