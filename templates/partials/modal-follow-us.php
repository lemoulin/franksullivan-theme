<?php

use FrankSullivan\Assets;

?>

<div id="modal-follow-us" class="modal is-from-bottom">

  <div class="modal-wrapper">

    <header class="modal-header">
      <a href="#" class="btn--close close-modal is-inverted">X</a>
    </header>

    <div class="modal-content">

      <ul class="social-links--circle">

        <?php if (get_field('facebook_url', 'options')): ?>
        <li>
          <a href="<?php the_field('facebook_url', 'options') ?>" target="_blank" class="social-links--facebook"><i class="ion-social-facebook"></i></a>
        </li>
        <?php endif; ?>

        <?php if (get_field('instagram_url', 'options')): ?>
        <li>
          <a href="<?php the_field('instagram_url', 'options') ?>" target="_blank" class="social-links--instagram"><i class="ion-social-instagram-outline"></i></a>
        </li>
        <?php endif; ?>

        <?php if (get_field('twitter_url', 'options')): ?>
          <li>
            <a href="<?php the_field('twitter_url', 'options') ?>" target="_blank" class="social-links--twitter"><i class="ion-social-twitter"></i></a>
          </li>
        <?php endif; ?>

      </ul>

    </div>

  </div>

  <div class="modal-background"></div>

</div>
