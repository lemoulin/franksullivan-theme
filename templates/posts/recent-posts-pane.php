<?php

use FrankSullivan\PostQueries;

$recent_posts = PostQueries\Query('get_posts', 3, $post);

?>

<div class="container-fluid posts--list">

  <hr class="container">

  <header>
    <h4 class="title is-5"><?php pll_e("next articles") ?></h4>
  </header>

  <div class="columns is-multiline">

    <?php $i=0; while ( $recent_posts->have_posts() ) : $recent_posts->the_post(); ?>
    <div class="column is-4" data-aos="fade-up" data-aos-duration="1250" data-aos-delay="<?= object_transition_stagger_speed($i, 150, 400, 4) ?>">
      <?php get_template_part('templates/posts/post-preview-pane'); ?>
    </div>
    <?php $i++; endwhile; ?>

  </div>

</div>
