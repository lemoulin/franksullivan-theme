<?php

use FrankSullivan\PostQueries;

$recent_posts      = PostQueries\Query('get_posts', 2);

?>

<footer class="posts--list-recent container">
  <h6 class="posts--list-recent--label title is-6"><?php pll_e("From our design crew") ?></h6>
  <div class="columns">
  <?php $i = 1; while ($recent_posts->have_posts()) : $recent_posts->the_post(); ?>
    <aside class="column is-4" data-aos="fade-up" data-aos-duration="1250" data-aos-delay="<?= object_transition_stagger_speed($i, 250, 50, 6) ?>">
      <?php get_template_part('templates/posts/post-preview-square'); ?>
    </aside>
  <?php $i++; endwhile; wp_reset_postdata(); ?>
  </div>
</footer>
