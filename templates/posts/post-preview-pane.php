<a href="<?php the_permalink() ?>" class="post--preview post--preview-pane">
  <figure>
    <?php echo get_responsive_image(null, 'large', 'bg') ?>
  </figure>
  <footer>
    <h4 class='title is-3'><?php echo the_title() ?></h4>
    <nav>
      <p class="link--learn-more"><?php pll_e('Learn more') ?> <i class="ion-ios-arrow-right"></i></p>
    </nav>
  </footer>
</a>
