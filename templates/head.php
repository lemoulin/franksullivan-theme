<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <?php wp_head(); ?>

  <script type="text/javascript">
  // constant
  var LANGUAGE = "<?= pll_current_language('slug') ?>";
  </script>

</head>
