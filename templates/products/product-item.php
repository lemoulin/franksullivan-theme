<?php

use FrankSullivan\Assets;

// global $i;

// get fields
$product_layout = get_field('product_layout');
$product_image_gallery = get_field('product_image_gallery');

// handles columns
switch ($product_layout) {
  case 'half-width':
    $rellax = false;
    $column = "is-6";
    $inside_columns = "is-12";
    break;
  case 'full-width':
    $rellax = false;
    $column = "is-12";
    $inside_columns = "is-6";
    break;
  default:
    $rellax = true;
    $column = "is-12";
    $inside_columns = "is-half";
    break;
}

?>

<article id="product-<?php the_ID() ?>" data-spy-in-viewport="<?php the_permalink() ?>" <?php post_class('column product--item ' . $product_layout . ' ' . $column); ?>>

  <div class="columns is-multiline">

      <?php echo $i; ?>

    <?php if (has_post_thumbnail()): ?>

    <figure class="column layout-box--post-thumbnail <?= $inside_columns ?>" data-aos="fade" data-aos-duration="1500" data-aos-delay="250" data-aos-offset="150">
      <?php if ($rellax): ?>
      <div class="_rellax" _data-rellax-speed="<?= object_transition_stagger_speed($i, 3, 0.25, 2, '') ?>">
      <?php endif; ?>
        <?= get_responsive_image(null, 'large', 'inline') ?>
      <?php if ($rellax): ?>
      </div>
      <?php endif; ?>
    </figure>

    <?php endif; ?>

    <?php if ($product_layout == "full-width"): ?>

      <?php if ($product_image_gallery): ?>
      <div class="column is-12">
        <?= franksullivan_custom_gallery_markup($product_image_gallery) ?>
      </div>
      <?php endif; ?>

      <aside class="column layout-box--description <?= $inside_columns ?>" data-aos="fade" data-aos-duration="1500" data-aos-delay="350" data-aos-offset="150">
        <h3 class="title is-1"><?php echo $i; ?> <?php the_title() ?></h3>
      </aside>

    <?php endif; ?>

    <aside class="column layout-box--description <?= $inside_columns ?>" data-aos="fade" data-aos-duration="1500" data-aos-delay="350" data-aos-offset="150">

      <?php // title is not needed in full width mode ?>
      <?php if ($product_layout != "full-width"): ?>
      <h3 class="title is-1"><?php echo $i; ?> <?php the_title() ?></h3>
      <?php endif ?>

      <?php // always output content ?>
      <?php the_content() ?>

      <?php // output gallery only when not in full-width mode  ?>
      <?php if ($product_image_gallery && $product_layout != "full-width"): ?>
      <h6 class="title is-6" data-aos="fade-right" data-aos-duration="1200" data-aos-delay="150"><?php pll_e("See more") ?></h6>
      <div class="is-hidden-mobile">
        <?= franksullivan_custom_thumbnail_gallery($product_image_gallery, $post->post_name) ?>
      </div>
      <div class="is-hidden-desktop">
        <?= franksullivan_custom_slider_gallery($product_image_gallery, "layout-box--slider is-slider-mobile") ?>
      </div>
      <?php endif; ?>
    </aside>

  </div>

</article>
