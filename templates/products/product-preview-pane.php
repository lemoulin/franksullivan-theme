<article class="product--preview-pane">
  <div class="box--content">
    <div class="product--preview-pane--hover level">
      <h4 class='title is-2 level-left'><?php echo the_title() ?></h4>
      <nav class="level-right">
        <h6 class="title is-6 btn--learn-more"><?php pll_e('Learn more') ?></h6>
      </nav>
    </div>
    <?php echo get_responsive_image(null, 'large', 'bg') ?>    
  </div>
</article>
