
<footer id="site-footer" class="content-info">
  <div class="container-fluid">
    <div class="columns is-multiline">
      <aside class="column is-3" data-aos="fade" data-aos-duration="1000" data-aos-delay="300">
        <figure class="footer--logo"></figure>
      </aside>
      <aside class="column is-narrow is-pull-right" data-aos="fade" data-aos-duration="1000" data-aos-delay="400">

        <div class="columns">

          <nav class="column is-7">
            <?php
            if (has_nav_menu('primary_navigation')) :
              wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => '']);
            endif;
            ?>
          </nav>

          <nav class="column ">
            <?php
            if (has_nav_menu('social_links')) :
              wp_nav_menu(['theme_location' => 'social_links', 'menu_class' => '']);
            endif;
            ?>
          </nav>

          <nav class="column ">
            <ul>
              <?php pll_the_languages(array('display_names_as' => 'slug', 'hide_current' => true)) ?>
              <li><span class="site-credits"></span></li>
            </ul>
          </nav>

        </div>


      </aside>
    </div>
  </div>
</footer>
