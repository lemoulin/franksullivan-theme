<?php

use FrankSullivan\Assets;

global $i;

// get fields
$expertise_layout = get_field('expertise_layout');
$expertise_image_gallery = get_field('expertise_image_gallery');

// handles columns
switch ($expertise_layout) {
  case 'half-width':
    $rellax = false;
    $column = "is-6";
    $inside_columns = "is-12";
    break;
  case 'full-width':
    $rellax = false;
    $column = "is-12";
    $inside_columns = "is-6";
    break;
  default:
    $rellax = true;
    $column = "is-12";
    $inside_columns = "is-half";
    break;
}

?>

<article id="expertise-<?php the_ID() ?>" <?php post_class('column expertise--item ' . $expertise_layout . ' ' . $column); ?>>

  <div class="columns is-multiline">

    <?php if (has_post_thumbnail()): ?>

    <figure class="column layout-box--post-thumbnail <?= $inside_columns ?>">
      <?php if ($rellax): ?>
      <div class="rellax" data-rellax-speed="<?= object_transition_stagger_speed($i, 4, 0.55, 2, '') ?>">
      <?php endif; ?>
        <?= get_responsive_image(null, 'large', 'inline') ?>
      <?php if ($rellax): ?>
      </div>
      <?php endif; ?>
    </figure>

    <?php endif; ?>

    <?php if ($expertise_layout == "full-width"): ?>

    <?php if ($expertise_image_gallery): ?>
    <div class="column is-12">
      <?= franksullivan_custom_gallery_markup($expertise_image_gallery) ?>
    </div>
    <?php endif; ?>

    <?php if ( get_the_title() ): ?>
    <aside class="column layout-box--description <?= $inside_columns ?>" data-aos="fade" data-aos-duration="2000" data-aos-delay="<?= object_transition_stagger_speed($i, 200, 1000, 5) ?>">
      <h3 class="title is-1"><?php the_title() ?></h3>
    </aside>
    <?php endif; ?>

    <?php endif; ?>

    <?php if (get_the_title() && get_the_content()): ?>

    <aside class="column layout-box--description <?= $inside_columns ?>" data-aos="fade" data-aos-duration="2000" data-aos-delay="<?= object_transition_stagger_speed($i, 200, 1000, 5) ?>">

      <?php // title is not needed in full width mode ?>
      <?php if ($expertise_layout != "full-width"): ?>
      <h3 class="title is-1"><?php the_title() ?></h3>
      <?php endif ?>

      <?php // always output content ?>
      <?php the_content() ?>

      <?php // output gallery only when not in full-width mode  ?>
      <?php if ($expertise_image_gallery && $expertise_layout != "full-width"): ?>
      <h6 class="title is-6"><?php pll_e("Selected by our designer") ?></h6>
      <?= franksullivan_custom_thumbnail_gallery($expertise_image_gallery, $post->post_name) ?>
      <?php endif; ?>

    </aside>

    <?php endif; ?>

  </div>

</article>
