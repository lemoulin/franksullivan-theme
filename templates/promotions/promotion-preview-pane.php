<?php
// get fields
$promotion_product  = get_field('promotion_product');
$promotion_post     = get_field('promotion_post');
$promotion_image    = get_field('promotion_image');
$promotion_column   = get_field('promotion_column');
$permalink          = get_field('promotion_url');

// set product as $post
if ($promotion_product) {
  $post = $promotion_product;
  $permalink = get_the_permalink($promotion_product);
}

if ($promotion_post) {
  $post = $promotion_post;
  $permalink = get_the_permalink($promotion_post);
}

?>

<a href="<?= $permalink ?>" class="promotion--pane <?= $promotion_column ?> loading-content">
  <div class="box--content">
    <div class="promotion--pane--hover level">
      <h4 class='title is-2 level-left'><?php echo the_title() ?></h4>
      <nav class="level-right">
        <h6 class="title is-6 link--learn-more"><?php pll_e('Learn more') ?> <i class="ion-ios-arrow-right"></i></h6>
      </nav>
    </div>
    <figure>
      <?php if ($promotion_image): ?>
      <?php echo get_responsive_image($promotion_image, 'largest', 'bg') ?>
      <?php else: ?>
      <?php echo get_responsive_image(null, 'largest', 'bg') ?>
      <?php endif; ?>
    </figure>
  </div>
</a>
