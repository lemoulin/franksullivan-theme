<?php

use FrankSullivan\PostQueries;

?>

<?php while (have_posts()) : the_post(); ?>

<?php $recent_posts = PostQueries\Query('get_posts', -1, $posts); ?>

  <article <?php post_class('post-single'); ?>>

    <header class="section--compact-header post--header">

      <hgroup class="container-narrow">
        <h1 class="title is-1"><?php the_title() ?></h1>
        <p class="post--meta">
          <span class="author"><?php pll_e("By") ?> <?php the_author() ?></span>
          <time><?php the_date() ?></time>
        </p>
      </hgroup>

      <aside class="section--compact-header--right-sidebar">
        <?php get_template_part('templates/partials/share-buttons') ?>
      </aside>

      <figure class="loading-content">
        <div class="rellax" data-rellax-speed="-2.5">
          <?php echo get_responsive_image(null, 'largest', 'bg') ?>
        </div>
      </figure>

    </header>

    <div class="post--content container-narrow">
      <?php the_content() ?>
    </div>

  </article>
<?php endwhile; ?>

<?php get_template_part('templates/partials/newsletter') ?>

<?php get_template_part('templates/posts/recent-posts-pane') ?>
