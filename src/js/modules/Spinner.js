import Spinner from '!!spin/dist/spin.min';

/*
* Add a spin.js to an element
*/
export function addSpinner(el) {
    var opts = {
        lines: 11,
        length: 20,
        width: 6,
        radius: 72,
        scale: 0.25,
        corners: 0.9,
        color: '#e5e5e5',
        opacity: 0.25,
        rotate: 0,
        direction: 1,
        speed: 1,
        trail: 60,
        fps: 20,
        zIndex: 2e9,
        className: 'spinner',
        top: '50%',
        left: '50%',
        shadow: false,
        hwaccel: true,
        position: 'absolute',
    }
    // let target = document.getElementById('foo')
    let spinner = new Spinner(opts).spin(el);
}
