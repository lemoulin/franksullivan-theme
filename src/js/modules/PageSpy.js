import inViewport from 'in-viewport';
import { debounce } from 'lodash';

// const spyElements = "[data-spy-in-viewport]";

class PageSpy {

  constructor() {
    this.init();
  }

  init() {
    this.spy();
  }

  spy() {
    // initial location
    let currentLocation = window.location.href;

    let debounced = debounce(() => {
      let visibles = [];

      // find all visible
      $("[data-spy-in-viewport]").each((i, e) => {
        let visible = inViewport(e, { offset: -500 });
        visible ? visibles.push(e) : null;
      });

      // if one is visible, replace state with this URL
      // return to original state if none are visible
      if (visibles.length > 0) {
        let url = $(visibles[0]).data('spy-in-viewport');
        this.replaceHistory(url);
      } else {
        this.replaceHistory(currentLocation);
      }

    }, 500);

    // attach to window scroll
    $(window).on('scroll', debounced);


    // on load, try to scroll to current element
    let $element = $(`[data-spy-in-viewport='${currentLocation}']`);
    if ( $element.get(0) ) {
      $("html,body").animate({ scrollTop: $element.offset().top }, 500);
    }
  }

  replaceHistory(newURL) {
    let currentURL = window.location.href;
    // console.log(newURL, currentURL);
    if (newURL !== currentURL) {
      // console.warn("URL has changed!");
      window.history.replaceState({}, null, newURL);
    }
  }

}

export default PageSpy;
