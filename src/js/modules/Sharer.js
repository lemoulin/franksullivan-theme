import domready from 'domready';

class Sharer {

	constructor() {
		this.init();
	}

	init() {
		this.setUrls();
		this.bindEvents();
	}

	// bind events to module elements
	bindEvents() {

		// facebook share
		$(document).on('click', "[data-share-facebook]", (event) => {
			event.preventDefault();
			let element = event.currentTarget;

			var width  = 575,
			height = 400,
			left   = ($(window).width()  - width)  / 2,
			top    = ($(window).height() - height) / 2,
			url    = element.href,
			opts   = 'status=1' +
			',width='  + width  +
			',height=' + height +
			',top='    + top    +
			',left='   + left;

			window.open(url, 'facebook', opts);

			return false;
		});

		// twitter share
		$(document).on('click', "[data-share-twitter]", (event) => {
			event.preventDefault();
			let element = event.currentTarget;

			var width  = 575,
			height = 400,
			left   = ($(window).width()  - width)  / 2,
			top    = ($(window).height() - height) / 2,
			url    = element.href,
			opts   = 'status=1' +
			',width='  + width  +
			',height=' + height +
			',top='    + top    +
			',left='   + left;

			window.open(url, 'twitter', opts);

			return false;
		});

		// linkedin
		// $("a.linkedin-share").on('click', (event) => {
		// 	event.preventDefault();
		// 	let element = event.currentTarget;
		//
		// 	var width  = 575,
		// 	height = 400,
		// 	left   = ($(window).width()  - width)  / 2,
		// 	top    = ($(window).height() - height) / 2,
		// 	url    = element.href,
		// 	opts   = 'status=1' +
		// 	',width='  + width  +
		// 	',height=' + height +
		// 	',top='    + top    +
		// 	',left='   + left;
		//
		// 	window.open(url, 'linkedin', opts);
		//
		// 	// close modal
		// 	this.toggle();
		//
		// 	return false;
		// });

	}

	getFacebookParams(element) {
		let data = {
			t: this.getTitle(element),
			u: encodeURI(this.getURL(element))
		};
		return $.param( data );
	}

	getTwitterParams(element) {
		let data = {
			text: this.getTitle(element),
			url: encodeURI(this.getURL(element))
		};
		return $.param( data );
	}

	getLinkedInParams(element) {
		let data = {
			mini: true,
			title: this.getTitle(element),
			url: encodeURI(this.getURL(element))
		};
		return $.param( data );
	}

	getTitle(element) {
		return element.getAttribute('title') ? element.getAttribute('title') : document.title;
	}

	getURL(element) {
		return element.getAttribute('href') ? element.getAttribute('href') : document.location.href;
	}

	setUrls() {
		$(document).find("[data-share-facebook]").not('[data-init]').each( (i, element)=> {
			$(element).attr('data-init', true).attr("href", "https://www.facebook.com/sharer/sharer.php?" + this.getFacebookParams(element));
		});
		$(document).find("[data-share-twitter]").each( (i, element)=> {
			$(element).attr('data-init', true).attr("href", "https://twitter.com/intent/tweet?" + this.getTwitterParams(element));
		});
	}

}

export default Sharer;
