
const forms         = "form[action*='list-manage.com']";
const formSuccessId = ".form-sucess"

class MailChimp {

  constructor() {
    // class objects
    this.a = null;
    this.creditsLabel = null;
    this.creditsName = null;

    // init
    this.init();
  }

  init() {
    this.forms();
  }

  // forms() {
  //
  // }

  forms(data) {
    // console.log($(forms));

    $(forms).on('submit', (event) => {
      event.preventDefault();

      // get content
      let $f = $(event.currentTarget);

      $.ajax({
        type: "GET",
        url: $f.attr("action"),
        data: $f.serialize(),
        cache: false,
        dataType: "jsonp",
        jsonp: "c",
        contentType: "application/json; charset=utf-8",
        success: (data) => {
          this.parseResponse($f, data);
        }
      });

    });
  }

  parseResponse(form, data) {
    console.log(data);

    // remove old messages first
    $(form).find('.mailchimp-message').remove();

    // get message
    let message = data.msg || "Mailchimp...";

    // create container
    let $container = $("<div/>").addClass('mailchimp-message').html(message);

    // set as error or success
    if (data.result != "success") {
      $container.addClass('error');
    } else {
      $container.addClass('success');
    }

    // append container to form
    $container.appendTo( form );

  }

}

export default MailChimp;

// show sucess message, or handles errors
// let formSuccess = (data) => {
//   if (data.result != "success") {
//       var message = data.msg || "Error";
//       $(formSuccessId).html(message).fadeIn();
//   } else {
//     $(formId).fadeOut(250, () => {
//       $(formSuccessId).html('Merci! /<br>Thank you!').fadeIn();
//     });
//   }
// }
//
// // handles form submit
// $(formId).on('submit', (event) => {
//   event.preventDefault();
//
//   // get content
//   let $f = $(event.currentTarget),
//       url = $f.attr('action'),
//       params = $f.serialize();
//
//   $.ajax({
//     type: "GET",
//     url: $f.attr("action"),
//     data: $f.serialize(),
//     cache: false,
//     dataType: "jsonp",
//     jsonp: "c", // trigger MailChimp to return a JSONP response
//     contentType: "application/json; charset=utf-8",
//     success: function(data){
//       formSuccess(data);
//     }
//   });
//
// });
