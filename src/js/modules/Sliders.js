import domready from 'domready';
import Swiper from 'swiper';
import 'core/Breakpoints';
import { wowInstance } from 'components/Figures';

const sliderDesktopSelector = '.is-slider';
const sliderMobileSelector  = '.is-slider-mobile';

class Sliders {

  constructor() {
    // class objects
    this.registredSliders = [];

    // init when dom is ready
    domready( ()=> {
      this.init();
    });
  }

  init() {

    // init tablet and mobile
    Breakpoints.on('touch', {
      enter: () => {
        this.mobile();
      },
      leave: () => {
        // this.destroy();
      }
    });

    // init desktop and beyond
    Breakpoints.on('desktop', {
      enter: () => {
        this.desktop();
      },
      leave: () => {
        // this.destroy();
      }
    });
  }

  desktop() {
    this.destroy();
  }

  mobile() {
    // destroy before creating
    this.destroy();

    $(sliderMobileSelector).each((i, e) => {
      // the slider container
      let $e = $(e);

      // set as swipert container
      $e.addClass('swiper-container');

      // create swiper wrapper
      let $swiperWrapper = $('<div/>').addClass('swiper-wrapper').appendTo( $e );

      // create pagination container
      let $swiperPanination = $('<div/>').addClass('swiper-pagination').appendTo( $e )

      // find all slides, add class, and append to container
      let $slides = $(e).find('.is-slide').addClass('swiper-slide').removeClass('column').appendTo( $swiperWrapper );

      let slider = new Swiper($e, {
        speed: 250,
        loop: true,
        grabCursor: true,
        slidesPerView: 1,
        pagination: $slides.length > 1 ? $swiperPanination : null,
        autoHeight: $e.data('slider-autoheight') ? true : false
      });

      // push to global array of sliders
      this.registredSliders.push(slider);
    });
  }

  destroy() {

    for (var i = 0; i < this.registredSliders.length; i++) {
      let $container = this.registredSliders[i].container;

      // add to original container
      $(this.registredSliders[i].slides).removeClass('swiper-slide').addClass('column').appendTo( $container );

      // remove classes on container
      $container.removeClass('swiper-container');

      // remove swipe duplicates
      $(this.registredSliders[i].slides).filter('.swiper-slide-duplicate').remove();

      // remove swiper wrapper
      $(this.registredSliders[i].wrapper).remove();

      // destroy
      this.registredSliders[i].destroy(false, true);
    }

    // re init array
    this.registredSliders = [];
  }

}

export default Sliders;
