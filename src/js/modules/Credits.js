import Handlebars from 'handlebars/dist/handlebars';
import { StyleSheet, css } from 'aphrodite/no-important';

// the container ID
const creditsContainerClassName = "site-credits";

// empty styles
let styles = styles = StyleSheet.create({
  // base A container style
  base: {
    display: 'block',
    width: '150px',
    height: '30px',
    textAlign: 'left',
    position: 'relative',
    '@media (max-width: 767px)': {
      textAlign: 'left',
    }
  },
  // labels container
  container: {
    display: "block",
    overflow: "hidden",
    position: "relative",
    height: "100%"
  },
  // transitions
  transitions: {
    '-moz-transition': 'all 350ms cubic-bezier(0.64, 0.57, 0.67, 1.53)',
    '-webkit-transition': 'all 350ms cubic-bezier(0.64, 0.57, 0.67, 1.53)',
    'transition': 'all 350ms cubic-bezier(0.64, 0.57, 0.67, 1.53)'
  },
  // default style for each text labels
  label: {
    display: 'block',
    width: "100%",
    position: "absolute",
    left: 0,
    top: 0,
    opacity: 1,
    transform: 'translateX(0)'
  },
  // credit name
  name: {
    display: 'block',
    width: "100%",
    position: "absolute",
    left: 0,
    top: 0,
    color: '#000000',
    opacity: 0,
    transform: 'translateX(50px)'
  },
  // popup panel
  pane: {
    position: 'absolute',
    top: "50%",
    left: 0,
    width: "120%",
    background: '#3325F1',
    color: '#fff',
    padding: "12px 15px 12px 57px",
    borderRadius: "30px",
    pointerEvents: "none",
    fontSize: '14px',
    lineHeight: '1',
    fontWeight: 'bold',
    opacity: 0,
    transitionDelay: '350ms',
    transitionDuration: '125ms',
    transform: 'translateX(-80%) translateY(-65%)',
    '@media (max-width: 767px)': {
      display: 'none',
    }
  },
  svg: {
    width: '24px',
    height: '24px',
    display: 'block',
    position: 'absolute',
    top: '18%',
    left: '22px',
    backgroundSize: 'contain',
    backgroundRepeat: 'no-repeat',
    backgroundImage: `url("data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iNThweCIgaGVpZ2h0PSI1NHB4IiB2aWV3Qm94PSIwIDAgNTggNTQiIHZlcnNpb249IjEuMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+CiAgICA8IS0tIEdlbmVyYXRvcjogU2tldGNoIDQ1LjIgKDQzNTE0KSAtIGh0dHA6Ly93d3cuYm9oZW1pYW5jb2RpbmcuY29tL3NrZXRjaCAtLT4KICAgIDxkZXNjPkNyZWF0ZWQgd2l0aCBTa2V0Y2guPC9kZXNjPgogICAgPGRlZnM+PC9kZWZzPgogICAgPGcgaWQ9IlBhZ2UtMSIgc3Ryb2tlPSJub25lIiBzdHJva2Utd2lkdGg9IjEiIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCI+CiAgICAgICAgPHBvbHlnb24gaWQ9Ik0iIGZpbGw9IiNGRkZGRkYiIHBvaW50cz0iNDIuNTkyMTIgMC45OTA0NTQ3NjcgMjkuOTU4OTEyMyAzMC45Mzg1ODgzIDI4Ljk5Mjg0MzUgMzMuNzYyNDgxOCAyOC4xMDEwODc3IDMwLjkzODU4ODMgMTUuNDY3ODggMC45OTA0NTQ3NjcgMC4zODIzNDM3NSAwLjk5MDQ1NDc2NyAwLjM4MjM0Mzc1IDUzLjAwOTU0NTIgMTMuMjM4NDkwNCA1My4wMDk1NDUyIDEzLjIzODQ5MDQgMjQuMzk5MDQ1NSAxNC43MjQ3NTAxIDI5LjUyNjY0MTUgMjMuNDkzNjgyNSA1My4wMDk1NDUyIDM0LjU2NjMxNzUgNTMuMDA5NTQ1MiA0My4zMzUyNDk5IDI5LjUyNjY0MTUgNDQuODIxNTA5NiAyNC4zOTkwNDU1IDQ0LjgyMTUwOTYgNTMuMDA5NTQ1MiA1Ny42Nzc2NTYzIDUzLjAwOTU0NTIgNTcuNjc3NjU2MyAwLjk5MDQ1NDc2NyI+PC9wb2x5Z29uPgogICAgPC9nPgo8L3N2Zz4=")`
  },
  // when hovering
  hover: {
    ':hover .credits--label': {
      transform: 'translateX(50px)',
      opacity: 0
    },
    ':hover .credits--name': {
      transform: 'translateX(0)',
      opacity: 1
    },
    ':hover .credits--hover-pane': {
      transform: 'translateX(-110%) translateY(-65%)',
      opacity: 1
    }
  }
});

// Strings translations
const Translations = {
  "fr" : {
    "credits": "Crédits",
    "text": "Code & Design"
  },
  "en" : {
    "credits": "Credits",
    "text": "Code & Design"
  }
}


class Credits {

  constructor() {
    // class objects
    this.a = null;
    this.creditsLabel = null;
    this.creditsName = null;

    // init
    this.init();
  }

  init() {
    this.button();
  }

  button() {
    // 1 . Build source
    let source = `
        <a href="{{url}}" target="_blank" id="credits--handler" class="${css(styles.base)} ${css(styles.hover)}">
          <div class="${css(styles.container)}">
            <span class="credits--label ${css(styles.label)} ${css(styles.transitions)}">{{label}}</span>
            <span class="credits--name ${css(styles.name)} ${css(styles.transitions)}">{{name}}</span>
          </div>
        </a>
        `,
        data = {
          label: Translations[this.detectLanguage()]["credits"],
          text: Translations[this.detectLanguage()]["text"],
          url: "https://mill3.studio",
          name: "MILL3 Studio"
        },
        template = Handlebars.compile(source),
        html  = template(data);

    // 2. Inject HTML in page, fails silently if no elements in DOM

    try {
      // get all containers
      let creditsContainer = document.getElementsByClassName(creditsContainerClassName);
      // 2. only inject in last container found (bug when using page transitions with main container duplications)
      creditsContainer[creditsContainer.length - 1].innerHTML = html;
    } catch (e) {
      console.warn(`Credits element #${creditsContainerID} not found.`);
    }
  }

  // check if has a window constant LANGUAGE
  detectLanguage() {
    if(LANGUAGE) {
      return LANGUAGE;
    } else {
      return "en";
    }
  }

}


export default Credits;
