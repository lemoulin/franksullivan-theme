// lib
import domready from 'domready';

// import modules
import Accordion from "./Accordion";
import Modals from './Modals';
import Sliders from './Sliders';
import PageSpy from './PageSpy';
import Sharer from './Sharer';
import MailChimp from './MailChimp';
import Credits from './Credits';

const modules = {
  "Accordion": new Accordion(),
  "Modals": new Modals(),
  "Sliders": new Sliders(),
  "PageSpy": new PageSpy(),
  "Sharer": new Sharer(),
  "MailChimp": new MailChimp(),
  "Credits": new Credits()
};

// method for a global reinit
modules.init = () => {
  modules.Accordion.init();
  modules.Modals.init();
  modules.Sliders.init();
  modules.PageSpy.init();
  modules.Sharer.init();
  modules.MailChimp.init();
  modules.Credits.init();
}


export default modules;
