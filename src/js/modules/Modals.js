import domready from 'domready';

class Modals {

  constructor() {
    this.init();
  }

  init() {
    domready( ()=> {
      this.bindEvents();
    });
  }

  bindEvents() {

    // custom events
    let onModalOpen = new Event('onModalOpen');
    let onModalClose = new Event('onModalClose');

    // init all modals
    $('.modal').each((i, elem) => {
      // attached event
      elem.addEventListener('onModalOpen', (event) => { this.open(event); }, false);
      elem.addEventListener('onModalClose', (event) => { this.close(event) }, false);

      $(elem).find('.close-modal').each( (i, closeElement) => {
        $(closeElement).on('click', (event) => {
          event.preventDefault();
          elem.dispatchEvent(onModalClose);
        });
      });

      // ESCAPE key
      $(document).keydown( (e) => {
        if (e.keyCode == 27) {
          elem.dispatchEvent(onModalClose);
        }
      });

    });

    // open events
    $(document).on('click', '.open-modal', (event) => {
      event.preventDefault();

      let $e = $(event.currentTarget);
      let elementType = event.currentTarget.nodeName;
      let id = null;

      if (elementType == 'LI') {
        id = $e.find('a').attr('href').replace('#', '');
      } else if (elementType == 'A') {
        id = $e.attr('href').replace('#', '');
      }

      // find element and fire event
      document.getElementById(id).dispatchEvent(onModalOpen);
    });
  }

  open(event) {
    let elem = event.target;
    $(elem).addClass('is-active');
  }

  close(event) {
    let elem = event.target;
    $(elem).removeClass('is-active');
  }


}

export default Modals;
