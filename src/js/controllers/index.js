import Home from './Home';
import Products from './Products';
import Contact from './Contact';

// components index
const controllers = {
	'Home': Home,
	'Products': Products,
	'Contact': Contact
};

export default controllers;
