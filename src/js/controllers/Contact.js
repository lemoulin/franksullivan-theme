import { mapStyle } from "core/Settings";
import { mobileDetect } from "core/Mobile";
let markerImage = require("../../images/map-marker.png");

export default {

    init() {
      this.map();
    },

    map() {
      // variables
      let $mapContainer = $(".contact-page--map--gmap"),
          lat           = $mapContainer.data('lat') ? $mapContainer.data('lat') : 0,
          lng           = $mapContainer.data('lng') ? $mapContainer.data('lng') : 0;

      // map options
      let mapOptions =  {
        center: {
          lat: lat,
          lng: lng
        },
        zoom: 14,
        disableDefaultUI: true,
        panControl: false,
        scaleControl: false,
        scrollwheel: false,
        zoomControl: false,
        draggable: true,
        styles: mapStyle
      };

      // create the map
      let gmap = new google.maps.Map( $mapContainer.get(0), mapOptions );

      let center = new google.maps.LatLng(lat, lng);
      let icon   = new google.maps.MarkerImage(
        markerImage,
        new google.maps.Size(37, 46),
        new google.maps.Point(0, 0),
        new google.maps.Point(19, 46)
      );

      // build URL
      let markerURL = null;

      if (mobileDetect.os() == 'iOS') {
        markerURL = `http://maps.apple.com/?q=269+Boul+Labelle,+Rosemère,+QC+J7A+2H3&sll=${lat},${lng}&z=16&t=s`;
      } else {
        markerURL = `http://google.com/maps/search/269+Boul+Labelle,+Rosemère,+QC+J7A+2H3/@${lat},${lng},16z`;
      }

      // add marker
      let marker = new google.maps.Marker({
        position: center,
        icon: icon,
        map: gmap,
        url: markerURL
      });

      // go to google maps on click
      google.maps.event.addListener(marker, 'click', function() {
        window.location.href = this.url;
      });

    }


};
