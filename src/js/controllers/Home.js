import { debounce } from 'lodash';
import { mobileDetect } from "core/Mobile";
import { setHeaderInverted } from "components/Header";

export default {

  init() {
    setHeaderInverted();
  }


};
