import domready from 'domready';

class Nav {

  constructor() {
      this.init();
  }

  init() {
    domready( ()=> {
      this.burgerMobile();
      this.scrollTo();
      this.toggleVisible();
    });
  }

  burgerMobile() {
    $(".site-header--burger").on('click', (event)=> {
      event.preventDefault();
      $(event.currentTarget).toggleClass('to-cross');
      $("#site-header").toggleClass('mobile-nav-visible');
    });
  }

  scrollTo() {
    $('[data-scroll-to]').on('click', function(event) {
        event.preventDefault();
        let top = $( $(this).attr('href') ).offset().top;
        $("html,body").animate({ scrollTop: top }, 1000);
    });
  }

  toggleVisible() {
    $('[data-toggle-visibile]').on('click', (event) => {
        event.preventDefault();
        // find element to trigger visible class
        let target = document.querySelector( event.currentTarget.dataset.toggleVisibile );
        $(target).toggleClass('visible');

        // set trigger link as active
        $(event.currentTarget).toggleClass('active');
    });
  }

}

export default Nav;
