import Nav from './Nav';
import Header from './Header';
import Figures from './Figures';
import Titles from './Titles';

const components = {
  'Nav': new Nav(),
  'Header': new Header(),
  'Figures': new Figures(),
  'Titles': new Titles()
};

// method for a global reinit
components.init = () => {
  components.Nav.init();
  components.Header.init();
  components.Figures.init();
  components.Titles.init();
}


export default components;
