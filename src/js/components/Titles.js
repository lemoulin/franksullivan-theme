import domready from 'domready';
import wordwrap from 'wordwrapjs/es5/lib/wordwrapjs';
import { mobileDetect } from 'core/Mobile';

class Titles {

  constructor() {
    this.init();
  }

  init() {
    domready( ()=> {
      this.breaksTitles();
    });
  }

  breaksTitles() {
    $(".section--intro-header hgroup h1, .section--intro-header hgroup h2").each((i, e) => {
      // get raw text
      let text = $(e).text();
      let lines = [];

      // on mobile, split by senteces
      if ( mobileDetect.mobile() || mobileDetect.tablet() ) {
        lines = text.split('.');
      // everywhere else, by lines breaks
      } else {
        lines = text.split('\n');
      }

      // empty original container
      $(e).empty();

      // append each line wrapped with a <span>
      $(lines).each((i, line) => {
        let span = $("<span />").addClass("title-line").appendTo(e);
        let inner = $("<span />").addClass('title-line--inner').text(line).appendTo(span);
      });
    });
  }


}


export default Titles;
