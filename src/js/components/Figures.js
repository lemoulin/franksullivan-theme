import domready from 'domready';
import Blazy from 'blazy/blazy.js';
import AOS from 'aos';
import { forEach } from 'lodash';
import magnificPopup from 'magnific-popup';
import Rellax from "../vendors/rellax";

export const bLazyInstance = new Blazy({
  offset: 1500,
  loadInvisible: true
});

// export const wowInstance = new WOW();

class Figures {

  constructor() {
    this.init();
  }

  init() {
    domready( ()=> {
      this.lazyload();
      this.reveals();
      this.effects();
      this.videos();
      this.zoom();
    });
  }

  lazyload() {
    bLazyInstance.revalidate();
  }

  reveals() {
    // wowInstance.init();
    AOS.init({
      offset: 0,
      once: true
    });
  }

  effects() {
    let r = new Rellax('.rellax', {
      wrapper: $(".barba-container:last").get(0),
      center: true
    });
  }

  videos() {
    let elements = document.querySelectorAll('.bg-video-filled');
    forEach(elements, (e, i) => {
      let video = e.querySelector('video');
      $(video).on('canplay', (event) => {
        $(e).addClass('ready');
      });
    });
  }

  zoom() {

    $('[data-gallery-zoom]').each(function() { // the containers for all your galleries
        $(this).magnificPopup({
            delegate: 'a',
            type: 'image',
            gallery: {
              enabled:true,
              tCounter: '%curr% | %total%'
            },
            mainClass: 'mfp-with-zoom',
            image: {
              titleSrc: 'title',
              markup: '<figure class="mfp-figure">'+
                        '<header class="mfp-header">'+
                        ' <div class="mfp-close"></div>'+
                        ' <p class="mfp-title"></p>'+
                        ' <div class="mfp-counter"></div>'+
                        '</header>'+
                        '<picture class="mfp-picture"><div class="mfp-img"></picture></div>'+
                      '</figure>',
            },
            zoom: {
              enabled: true,
              duration: 275,
              easing: 'ease-in-out',
              opener: function(openerElement) {
                return openerElement.is('img') ? openerElement : openerElement.find('img');
              }
            },
            callbacks: {
              beforeClose: () => {
                // fade out elements before closing
                $('.mfp-bg').removeClass('mfp-ready')
              }
            }
        });
    });
  }

}


export default Figures;
