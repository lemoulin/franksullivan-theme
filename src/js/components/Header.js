import domready from 'domready';
import { debounce } from 'lodash';
import scrollDir from "bloody-scroll-direction";
import moment from 'moment';
import lscache from 'lscache';
import 'core/Breakpoints';
import { __ } from "core/Translations";

// selector for tablet and mobile device
let siteHeaderSelector     = ".site-header";

// selector for desktop and beyond
let siteHeaderNavSelector  = ".site-header--nav";

// opening hours bar
let openingHoursSelector   = ".site-header--opening-hours";

// maxscroll before setting as hidden
let maxScrollY             = 100;

// burger menu
let toggleNavMobileSelector    = ".toggle-nav-mobile";
let mobileMenuContentSelector  = ".nav-primary--mobile";


class Header {

  constructor() {
    this.init();
  }

  init() {
    domready( ()=> {
      this.toggleState();
      this.openingHours();
      this.burgerMobile();
    });
  }

  toggleState() {
    let scrollDirection = scrollDir.create();
    let wasInverted = false;
    let defaultminTopScroll = $(openingHoursSelector).outerHeight();

    // create debounce event
    let debounced = debounce(() => {
      // set sticky when opening hours is outside viewport
      let $header     = currentHeaderSelector();
      let visible     = $(window).scrollTop() < defaultminTopScroll - 5;
      let inverted    = $header.hasClass('is-inverted');
      let direction   = scrollDirection._direction;

      // set constant to true once
      if ( inverted ) {
        wasInverted = true;
      }

      if ( !visible && direction > 0 ) {

        $header
          .addClass("is-sticky")
          .addClass("is-hide-up");

        (wasInverted) ? $header.removeClass('is-inverted') : "";

      } else if( !visible && direction < 0 ) {

        $header
          .addClass("is-sticky")
          .removeClass("is-hide-up");

        (wasInverted) ? $header.removeClass('is-inverted') : "";

      } else if( visible ) {

        $header
          .removeClass("is-filled is-sticky is-hide-up");

        // return as inverted when need
        (wasInverted) ? $header.addClass('is-inverted') : "";

      }
    }, 1);

    // attach to window scroll
    $(window).on('scroll', debounced);
  }

  openingHours() {
    // open/close details panel
    let $toggle = $('.opening-hours--toggle');
    let $openHoursDetails = $(".opening-hours--details");

    $toggle.on('click', (event) => {
      event.preventDefault();
      /* Act on the event */
      $toggle.toggleClass('active');
      $openHoursDetails.toggleClass('open');
    });

    // Google Maps API
    let PlaceID = "ChIJf2QANlMmyUwRpho_95X0uy0";
    let PlacesService = new google.maps.places.PlacesService( document.createElement('div') );

    // try to get cached place data
    let CACHE_KEY = `CACHED_SERVICE_PLACE_${LANGUAGE}`;
    let CACHED_SERVICE_PLACE = lscache.get(CACHE_KEY);


    // if cached, use cache data
    if (CACHED_SERVICE_PLACE) {
      this.setOpeningHours(CACHED_SERVICE_PLACE);

    // Get data from PlacesService
    } else {
      // get place details
      PlacesService.getDetails({
        'placeId': PlaceID
      }, (place, status) => {
        if (status === google.maps.places.PlacesServiceStatus.OK) {
          // store and cache data for 30 minutes
          lscache.set(CACHE_KEY, place, 30);
          this.setOpeningHours(place);
        }
      });
    }
  }

  setOpeningHours(place) {
    let text;

    // containers
    let $openingHoursTextContainer = $('.opening-hours--toggle span');

    // get isoWeek day number, PlacesService returns : [0]Monday, [1]Tuesday, etc.
    let dayNumber = moment().isoWeekday() - 1;

    // check if is open right now
    if (place.opening_hours.open_now) {
      text = __('store_open');
      $openingHoursTextContainer.html(`<span class="is-hidden-mobile">${text} – </span>${place.opening_hours.weekday_text[dayNumber]}`);
    } else {
      text = __('store_closed');
      $openingHoursTextContainer.html(text);
    }
  }

  burgerMobile() {
      $(toggleNavMobileSelector).on('click touchstart', (event)=> {
        event.preventDefault();
        $(event.currentTarget).toggleClass('active');
        $(mobileMenuContentSelector).toggleClass('open');
      });
    }

}

// get current visible header .barba-container, which should be the last added in DOM
// also get different header if desktop or tablet
export const currentHeaderSelector = () => {
  if (Breakpoints.current().name == 'touch') {
    return $('.barba-container').filter(":last").find(siteHeaderSelector);
  } else {
    return $('.barba-container').filter(":last").find(siteHeaderNavSelector);
  }
}

// method for inverting menu
export const setHeaderInverted = () => {
  $( currentHeaderSelector() ).add(openingHoursSelector).addClass("is-inverted");
}

export default Header;
