import Breakpoints from 'breakpoints-js';

export default Breakpoints({
  touch: {
      min: 0,
      max: 999
  },
  desktop: {
      min: 1000,
      max: Infinity
  }
});
