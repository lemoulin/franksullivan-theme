import MobileDetect from "mobile-detect";
import domready from 'domready';

export const mobileDetect = new MobileDetect(window.navigator.userAgent);

export const setMobileClasses = () => {
  if (mobileDetect.mobile()) {
    $("body").addClass('mobile');
    $("body").addClass(mobileDetect.os());
  }
}

// set classes on load
setMobileClasses();
