
const Translations = {
  "en" : {
    "store_open": "We are open",
    "store_closed": "Now closed"
  },
  "fr" : {
    "store_open": "Nous sommes ouverts",
    "store_closed": "Fermé"
  }
}

// get translation string
export const __ = (slug) => {
  try {
    return Translations[LANGUAGE][slug]
  } catch (e) {
    console.warn(e, 'Translation not found or language setting is not set');
    return slug;
  };
}
