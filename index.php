<?php

use FrankSullivan\PostQueries;

// get header image
$header_image = get_field('products_header_image', 'options');

// get posts

// get the first post depend on archive or landing page of news
if ( is_category() ) {
  $first_post  = PostQueries\Query('get_posts', 1);
} else {
  $first_post  = PostQueries\Query('get_featured_posts', 1);
}

// get first post ID
try {
  $first_post_ID = $first_post->posts[0]->ID;
} catch (Exception $e) {
  $first_post_ID = null;
}


?>

<section class="posts--index">

  <!-- top post -->
  <?php while ( $first_post->have_posts() ) : $first_post->the_post(); ?>
  <header <?php post_class("section--compact-header"); ?>>

    <aside class="section--compact-header--left-sidebar">
      <ul class="categories--list is-unstyled">
        <?php wp_list_categories('title_li=') ?>
      </ul>
    </aside>

    <hgroup class="container-narrow post--header">
      <h1 class="title is-1"><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h1>
      <p class="post--meta">
        <span class="author"><?php pll_e("By") ?> <?php the_author() ?></span>
        <time><?php the_date() ?></time>
      </p>
      <div class="post--summary">
        <?php the_excerpt($post) ?>
      </div>
    </hgroup>

    <aside class="section--compact-header--right-sidebar is-hidden-touch">
      <?php get_template_part('templates/partials/share-buttons') ?>
    </aside>

    <figure class="loading-content">
      <a href="<?php the_permalink() ?>">
        <div class="rellax" data-rellax-speed="-4">
          <?php echo get_responsive_image(null, 'largest', 'bg') ?>
        </div>
      </a>
    </figure>


  </header>
  <?php endwhile; wp_reset_postdata(); ?>

  <div class="container-fluid posts--list">

    <hr class="container">

    <header>
      <h4 class="title is-5"><?php pll_e("next articles") ?></h4>
    </header>

    <div class="columns is-multiline">

      <?php $i=0; while ( have_posts() ) : the_post(); ?>
      <?php if ($post->ID != $first_post_ID): ?>
      <div class="column is-4" data-aos="fade-up" data-aos-duration="1250" data-aos-delay="<?= object_transition_stagger_speed($i, 150, 150, 4) ?>" data-aos-offset="-100">
        <?php get_template_part('templates/posts/post-preview-pane'); ?>
      </div>
      <?php endif; ?>
      <?php $i++; endwhile; ?>

    </div>

  </div>

</section>

<?php the_posts_navigation(); ?>
