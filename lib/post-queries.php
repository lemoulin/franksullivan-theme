<?php

namespace FrankSullivan\PostQueries;
use WP_Query;

class Theme_PostQueries {

  // variables
  private $limit;

  // init class
  function __construct($limit, $exclude = []) {
    // if not limit is set, get current site option
    $this->limit = $limit ? $limit : $this->post_per_page();

    // exclude posts from query
    $this->exclude = $this->build_exclude_list($exclude);
  }

  // get posts
  public function get_posts() {
    // global $wp_query;
    // print_r(get_query_var('cat'));
    $post = new WP_Query(array(
        'post_type'       => 'post',
        'lang'            => pll_current_language(),
        'posts_per_page'  => $this->limit,
        'post__not_in'    => $this->exclude,
        'category__in'    => get_query_var('cat')
    ));
    return $post;
  }

  // get posts
  public function get_featured_posts() {
    $post = new WP_Query(array(
        'post_type'           => 'post',
        'lang'                => pll_current_language(),
        'post__in'            => get_option( 'sticky_posts' ),
        'posts_per_page'      => $this->limit,
        'ignore_sticky_posts' => 1
    ));
    return $post;
  }

  // get promotions
  public function promotions() {
    $promotions = new WP_Query(array(
        'post_type'      => 'promotions',
        'lang'           => pll_current_language(),
        'posts_per_page' => $this->limit,
        'orderby'        => 'menu_order'
    ));
    return $promotions;
  }

  // get products
  public function products() {
    $products = new WP_Query(array(
        'post_type'      => 'products',
        'lang'           => pll_current_language(),
        'posts_per_page' => $this->limit,
        'orderby'        => 'menu_order'
    ));
	  return $products;
  }

  // // get carousel projects
  public function products_on_homepage() {
    $products = new WP_Query(array(
        'post_type'      => 'products',
        'lang'           => pll_current_language(),
        'posts_per_page' => $this->limit,
        'orderby'        => 'menu_order',
        'meta_key'       => 'product_show_on_homepage',
	      'meta_value'     => '1'
    ));
	  return $products;
  }

  // get per page option from WP settings
  private function post_per_page() {
    return get_option('posts_per_page');
  }

  // returns current language if using Polylang plugin
  private function language() {
    if (function_exists('pll_current_language')) {
      return pll_current_language();
    } else {
      return null;
    }
  }

  private function build_exclude_list($data) {
    if ( is_array($data) ) {
      return wp_list_pluck($data, 'ID', null);
    } else if ( is_object($data) ) {
      return array($data->ID);
    } else {
      return [];
    }
  }


}

// Public theme function for constructing a Query object from a template file
function Query($type, $limit = null, $exclude = null) {
  $query = new Theme_PostQueries($limit, $exclude);

  // $type is evaluated as a Class method
  return $query->$type();
}

?>
