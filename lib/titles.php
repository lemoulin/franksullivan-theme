<?php

namespace FrankSullivan\Titles;

/**
 * Page titles
 */
function title() {
  if (is_home()) {
    if (get_option('page_for_posts', true)) {
      return get_the_title(get_option('page_for_posts', true));
    } else {
      return pll_e('Latest Posts', 'franksullivan');
    }
  } elseif (is_archive()) {
    return get_the_archive_title();
  } elseif (is_search()) {
    return sprintf(pll__('Search Results for %s', 'franksullivan'), get_search_query());
  } elseif (is_404()) {
    return pll_e('Not Found', 'franksullivan');
  } else {
    return get_the_title();
  }
}
