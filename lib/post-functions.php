<?php

/**
 * Return <img/div> markup using WP's media image object array
 */
function get_responsive_image($image_object = null, $size = 'medium', $type = 'lazy', $pixel = true) {
  global $post;

  // handle image source
  if ( !$image_object && has_post_thumbnail() ) {
    $image = get_the_post_thumbnail_url($post);
  } else if ($image_object && isset($image_object['sizes'])) {
    $image = $image_object['sizes'][$size];
  } else {
    // no object and no thumbnail, returns nothing
    return false;
  }

  // with pixel image
  // if ($pixel) {
  //   $pixel_img = "<img src='{$image_object['sizes']['pixel']}' class='preload-pixel' />";
  // } else {
  //   $pixel_img = null;
  // }

  // output markup
  switch ($type) {
    case 'lazy':
      $img =  "<img src='{$pixel}' class='b-lazy' data-src='{$image}' />";
      break;
    case 'bg':
      $img =  "<picture class='b-lazy bg-filled' data-src='{$image}'></picture>";
      break;
    case 'inline':
      $img =  "<img src='{$image}' />";
  }

  return $img;
}

/**
 * Return <video> markup to use as a video background
 */
function get_background_video($src = '') {
  return "<div class='bg-video-filled'><video autoplay loop muted playsinline><source src='$src' type='video/mp4' /></video></div>";
}

/**
 * Custom oembed filter, add a wrapper for responsive embeded iframes
 */

function franksullivan_oembed_filter($html, $url, $attr, $post_ID) {
    $return = '<div class="embed-responsive"><div class="box--content">'.$html.'</div></div>';
    return $return;
}

add_filter( 'embed_oembed_html', 'franksullivan_oembed_filter', 10, 4 ) ;


/**
 * Returns row odd/even fraction
 */

function row_fraction($index) {
  return ($index % 2) == 1 ? 'odd' : 'even';
}


/**
 * Returns object transition speed
 */
function object_transition_stagger_speed($index = 0, $increment = 50, $base = 0,  $every = 3, $timing = '') {
  // recalculate index when reached X loop iteration
  $i = $index % $every;
  $speed = $base + ($i * $increment);
  return sprintf("%s%s", $speed, $timing);
}

/**
 * Check if on last page of a wp_query object
 */

function is_last_page() {
  global $wp_query;

  $page        = get_query_var('paged');
  $total_page  = $wp_query->max_num_pages;

  // return bool
  return ($page < $total_page) ? false : true;
}


/**
 * Change read more link
 */

function excerpt_more_custom() {
  global $post;
  return '<nav class="read-more"><a class="link--learn-more" href="' . get_permalink($post->ID) . '">' . pll__("Read more", "franksullivan") . '<i class="ion-ios-arrow-down"></i></a></nav>';
}

add_filter( 'excerpt_more', 'excerpt_more_custom' );


/**
 * Output gallery in a custom format
*/

function franksullivan_custom_gallery($output = '', $atts) {
  // get all gallery shortcode posts
  $posts = get_posts(array('include' => $atts['ids'], 'orderby' => $atts['orderby'], 'post_type' => 'attachment'));
  $total = count($posts);
  return franksullivan_custom_gallery_markup($posts);
}

add_filter('post_gallery','franksullivan_custom_gallery', 10, 2);

/**
 * Output custom gallery markup with grid options and all
*/

function franksullivan_custom_gallery_markup($posts) {
  // slider wrapper begin
  $output = "<div class='post--image-gallery columns is-multiline'>";

    // slide for each image
	foreach($posts as $key => $image) {
    // transition delay
    $delay = object_transition_stagger_speed($key, 150, 400, 4);
    $sub_delay = object_transition_stagger_speed($key, 150, 550, 4);

    // transform array to a WP Post object
    if ( is_array($image) ) {
      $image = get_post($image['id']);
    }

    // get custom fields
    $media_column = get_field('media_column', $image->ID);
    $media_full_width = get_field('media_full_width', $image->ID);
    $media_box_type = get_field('media_box_type', $image->ID);
    $media_box_ratio = get_field('media_box_ratio', $image->ID);
    $media_bg_fixed = get_field('media_bg_fixed', $image->ID) ? "bg-fixed" : "";
    $media_background_video = get_field('media_background_video', $image->ID);
    $media_text_on_image = get_field('media_text_on_image', $image->ID);

    // set column width
    if ($media_column) {
      $column = "column is-" . get_field('media_column', $image->ID);
    } else if (!$media_column && $media_full_width) {
      $column = "is-full-width";
    }

    // insert caption
    if ($image->post_excerpt) {
  		$caption = "<figcaption class='caption' data-aos='fade-right' data-aos-duration='1000' data-aos-delay='{$sub_delay}'><p>" . $image->post_excerpt . "</p></figcaption>";
      $caption_class = "is-with-caption";
  	} else {
      $caption = "";
      $caption_class = "";
    }

    // video
    if ($media_background_video) {
      $video = "<video autoplay loop muted playsinline><source src='$media_background_video' type='video/mp4' /></video>";
    } else {
      $video = "";
    }

    // text
    if ($media_text_on_image) {
      $text_overlay = "<header class='media-text-overlay'><p>{$media_text_on_image}</p></header>";
    } else {
      $text_overlay = "";
    }

  	// the <img> src
    $img_src = wp_get_attachment_image_src($image->ID, 'largest')[0];

    // handles box type
    if ($media_box_type == "bg") {
      $picture = "<div class='ratio-box {$media_box_ratio} {$caption_class}'><picture class='b-lazy bg-filled {$media_bg_fixed}' data-src='{$img_src}'>{$text_overlay}{$video}</picture></div>";
    } else {
      $picture = "<picture>{$text_overlay}<img src='{$img_src}'></picture>";
    }

  	// create and append html
    $output .= "<figure class='{$column}' data-aos='fade-up' data-aos-duration='1250' data-aos-delay='{$delay}'>{$picture}{$caption}</figure>";
  }

  $output .= "</div>";

  return $output;
}


/**
 * Output custom gallery as a grid of thumbnail
*/

function franksullivan_custom_thumbnail_gallery($posts, $modal_scope = '') {
  // slider wrapper begin
  $output = "<div class='post--gallery-thumbnail columns is-multiline' data-gallery-zoom>";

    // slide for each image
	foreach($posts as $key => $image) {
    // transition delay
    $delay = object_transition_stagger_speed($key, 50, 250, 4);

    // transform array to a WP Post object
    if ( is_array($image) ) {
      $image = get_post($image['id']);
    }

  	// the <img> src
    $img_src = wp_get_attachment_image_src($image->ID, 'thumbnail')[0];
    $img_src_largest = wp_get_attachment_image_src($image->ID, 'largest')[0];
    $picture = "<picture><a href='{$img_src_largest}' class='no-barba' data-zoom-image='{$modal_scope}' title='{$image->post_excerpt}'><img src='{$img_src}' data-src-hd='{$img_src_largest}' alt='{$image->post_title}'></a></picture>";

  	// create and append html
    $output .= "<figure class='column is-4' data-aos='fade' data-aos-duration='1200' data-aos-delay='{$delay}' data-aos-offset='100'>{$picture}{$caption}</figure>";
  }

  $output .= "</div>";

  return $output;
}

/**
 * Output custom gallery as a slider
*/

function franksullivan_custom_slider_gallery($posts, $className = 'is-slider') {
  // slider wrapper begin
  $output = "<div class='$className' data-slider-autoheight='true'>";

    // slide for each image
	foreach($posts as $key => $image) {
    // transition delay
    // $delay = object_transition_stagger_speed($key, 12, 1, 4);

    // transform array to a WP Post object
    if ( is_array($image) ) {
      $image = get_post($image['id']);
    }

  	// the <img> src
    $img_src = wp_get_attachment_image_src($image->ID, 'large')[0];
    $picture = "<picture><img src='{$img_src}' alt='{$image->post_title}'></picture>";

  	// create and append html
    $output .= "<figure class='is-slide'>{$picture}{$caption}</figure>";
  }

  $output .= "</div>";

  return $output;
}

/**
 * Output SEO title for custom post type archives
*/

function franksullivan_archive_title($title) {

  // products archives
  if ( is_post_type_archive ( 'products' ) ) {
    $title_raw = pll__('Products in store') . " %%sep%% %%sitename%%";
    return wpseo_replace_vars( $title_raw, array() );
  }
  // expertises archives
  if ( is_post_type_archive ( 'expertises' ) ) {
    $title_raw = pll__('Our expertises') . " %%sep%% %%sitename%%";
    return wpseo_replace_vars( $title_raw, array() );
  }
  // expertises archives
  if ( is_home () ) {
    $title_raw = pll__('Our inspirations') . " %%sep%% %%sitename%%";
    return wpseo_replace_vars( $title_raw, array() );
  }

  // default return
  return $title;

}

// change page title
add_filter('wpseo_title', 'franksullivan_archive_title', 10, 1);

?>
