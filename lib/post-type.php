<?php

namespace FrankSullivan\PostType;

/**
 * Creating a function to create our Custom Post Type
 */
class Theme_CustomPostTypes {

  public $theme_domain = 'franksullivan';
  public $menu_position = 3; // starting position in admin menu

  function __construct() {
    $this->promotions();
    $this->products();
    $this->expertises();
    $this->faqs();
  }

  private function incrementMenuPosition() {
    return $this->menu_position += 1;
  }

  // Create projects post-types
  public function promotions() {
    $labels = array(
        'name'                => pll__("Promotions"),
        'singular_name'       => pll__("Promotion"),
        'menu_name'           => __( 'Promotions', $this->theme_domain ),
        'parent_item_colon'   => __( 'Parent Promotion', $this->theme_domain ),
        'all_items'           => __( 'All Promotions', $this->theme_domain ),
        'view_item'           => __( 'View Promotion', $this->theme_domain ),
        'add_new_item'        => __( 'Add New Promotion', $this->theme_domain ),
        'add_new'             => __( 'Add New', $this->theme_domain ),
        'edit_item'           => __( 'Edit Promotion', $this->theme_domain ),
        'update_item'         => __( 'Update Promotion', $this->theme_domain ),
        'search_items'        => __( 'Search Promotion', $this->theme_domain ),
        'not_found'           => __( 'Not Found', $this->theme_domain ),
        'not_found_in_trash'  => __( 'Not found in Trash', $this->theme_domain ),
    );

    // Set other options for Custom Post Type
    $args = array(
        'label'               => __( 'promotions', $this->theme_domain ),
        'description'         => __( 'Promotion details', $this->theme_domain ),
        'labels'              => $labels,
        'supports'            => array( 'title', 'editor', 'revisions' ),
        'hierarchical'        => true,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => $this->incrementMenuPosition(),
        'can_export'          => true,
        'has_archive'         => false,
        'exclude_from_search' => true,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
        // 'yarpp_support'       => true,
        'menu_icon'           => 'dashicons-portfolio',
        'rewrite'             => array('slug' => 'promotions')
    );

    // Register
    register_post_type( 'promotions', $args );
  }

  // Create projects post-types
  public function products() {
    $labels = array(
        'name'                => pll__("Products"),
        'singular_name'       => pll__("Product"),
        'menu_name'           => __( 'Products', $this->theme_domain ),
        'parent_item_colon'   => __( 'Parent Product', $this->theme_domain ),
        'all_items'           => __( 'All Products', $this->theme_domain ),
        'view_item'           => __( 'View Product', $this->theme_domain ),
        'add_new_item'        => __( 'Add New Product', $this->theme_domain ),
        'add_new'             => __( 'Add New', $this->theme_domain ),
        'edit_item'           => __( 'Edit Product', $this->theme_domain ),
        'update_item'         => __( 'Update Product', $this->theme_domain ),
        'search_items'        => __( 'Search Product', $this->theme_domain ),
        'not_found'           => __( 'Not Found', $this->theme_domain ),
        'not_found_in_trash'  => __( 'Not found in Trash', $this->theme_domain ),
    );

    // Set other options for Custom Post Type
    $args = array(
        'label'               => __( 'products', $this->theme_domain ),
        'description'         => __( 'Product details', $this->theme_domain ),
        'labels'              => $labels,
        'supports'            => array( 'title', 'editor', 'thumbnail', 'revisions' ),
        'hierarchical'        => true,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => $this->incrementMenuPosition(),
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
        // 'yarpp_support'       => true,
        'menu_icon'           => 'dashicons-portfolio',
        'rewrite'             => array('slug' => 'products')
    );

    // Register
    register_post_type( 'products', $args );
  }

  // Create projects post-types
  public function expertises() {
    $labels = array(
        'name'                => pll__("Expertises"),
        'singular_name'       => pll__("Expertise"),
        'menu_name'           => __( 'Expertises', $this->theme_domain ),
        'parent_item_colon'   => __( 'Parent Expertise', $this->theme_domain ),
        'all_items'           => __( 'All Expertises', $this->theme_domain ),
        'view_item'           => __( 'View Expertise', $this->theme_domain ),
        'add_new_item'        => __( 'Add New Expertise', $this->theme_domain ),
        'add_new'             => __( 'Add New', $this->theme_domain ),
        'edit_item'           => __( 'Edit Expertise', $this->theme_domain ),
        'update_item'         => __( 'Update Expertise', $this->theme_domain ),
        'search_items'        => __( 'Search Expertise', $this->theme_domain ),
        'not_found'           => __( 'Not Found', $this->theme_domain ),
        'not_found_in_trash'  => __( 'Not found in Trash', $this->theme_domain ),
    );

    // Set other options for Custom Post Type
    $args = array(
        'label'               => __( 'expertises', $this->theme_domain ),
        'description'         => __( 'Expertise details', $this->theme_domain ),
        'labels'              => $labels,
        'supports'            => array( 'title', 'editor', 'thumbnail', 'revisions' ),
        'hierarchical'        => true,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => $this->incrementMenuPosition(),
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
        // 'yarpp_support'       => true,
        'menu_icon'           => 'dashicons-portfolio',
        'rewrite'             => array('slug' => 'expertises')
    );

    // Register
    register_post_type( 'expertises', $args );
  }

  // Create FAQ post-types
  public function faqs() {
    $labels = array(
        'name'                => pll__("FAQs"),
        'singular_name'       => pll__("FAQ"),
        'menu_name'           => __( 'FAQs', $this->theme_domain ),
        'parent_item_colon'   => __( 'Parent FAQ', $this->theme_domain ),
        'all_items'           => __( 'All FAQs', $this->theme_domain ),
        'view_item'           => __( 'View FAQ', $this->theme_domain ),
        'add_new_item'        => __( 'Add New FAQ', $this->theme_domain ),
        'add_new'             => __( 'Add New', $this->theme_domain ),
        'edit_item'           => __( 'Edit FAQ', $this->theme_domain ),
        'update_item'         => __( 'Update FAQ', $this->theme_domain ),
        'search_items'        => __( 'Search FAQ', $this->theme_domain ),
        'not_found'           => __( 'Not Found', $this->theme_domain ),
        'not_found_in_trash'  => __( 'Not found in Trash', $this->theme_domain ),
    );

    // Set other options for Custom Post Type
    $args = array(
        'label'               => __( 'faqs', $this->theme_domain ),
        'description'         => __( 'FAQ details', $this->theme_domain ),
        'labels'              => $labels,
        'supports'            => array( 'title', 'editor', 'revisions', 'thumbnail' ),
        'hierarchical'        => true,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => $this->incrementMenuPosition(),
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
        'menu_icon'           => 'dashicons-portfolio',
        'rewrite'             => array('slug' => 'faqs')
    );

    // Register
    register_post_type( 'faqs', $args );
  }



}

new Theme_CustomPostTypes();

add_filter('pll_translated_post_type_rewrite_slugs', function($post_type_translated_slugs) {
  $post_type_translated_slugs = array(
    'products' => array(
      'fr' => array(
        'has_archive' => true,
        'rewrite' => array(
          'slug' => 'produits',
        ),
      ),
      'en' => array(
        'has_archive' => true,
        'rewrite' => array(
          'slug' => 'products',
        ),
      ),
    ),
    'expertises' => array(
      'fr' => array(
        'has_archive' => true,
        'rewrite' => array(
          'slug' => 'savoir-faire',
        ),
      ),
      'en' => array(
        'has_archive' => true,
        'rewrite' => array(
          'slug' => 'expertises',
        ),
      ),
    )
  );

  return $post_type_translated_slugs;
});


?>
