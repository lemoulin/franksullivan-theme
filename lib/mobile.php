<?php

require_once __DIR__ . '/Mobile_Detect.php';

function isMobile() {
  $mobileDetect = new Mobile_Detect;
  return $mobileDetect->isMobile();
}

function isTablet() {
  $mobileDetect = new Mobile_Detect;
  return $mobileDetect->isTablet();
}

function isDesktop() {
  $mobileDetect = new Mobile_Detect;
  echo $mobileDetect->isDesktop();
  return $mobileDetect->isDesktop();
}
