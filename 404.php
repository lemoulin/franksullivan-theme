<?php

use FrankSullivan\Titles;

// get fields
$products_header_image = get_field('products_header_image', 'options');

 ?>

<section class="generic-page" data-controller="Page">

  <header class="section--compact-header">

    <hgroup class="container-narrow post--header">
      <h1 class="title is-1"><?= Titles\title(); ?></h1>
      <div class="post--summary">
        <p class="text-center">
          <?php pll_e('Sorry, but the page you were trying to view does not exist.', 'franksullivan'); ?>
        </p>
      </div>
    </hgroup>

    <figure class="loading-content">
      <div class="rellax" data-rellax-speed="-4">
        <?php echo get_responsive_image($products_header_image, 'largest', 'bg') ?>
      </div>
    </figure>


  </header>
</section>
