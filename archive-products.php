<?php
use FrankSullivan\Assets;
use FrankSullivan\PostQueries;

// get fields
$products_header_image = get_field('products_header_image', 'options');
$products_intro_text = get_field('products_intro_text_' . pll_current_language(), 'options');
$products_suppliers = get_field('products_suppliers_' . pll_current_language(), 'options');

// force to get all products
$posts = PostQueries\Query('products', -1);

// function franksullivan_products_archive_title($title) {
//   echo "FOOBAR!!!";
//   echo $title;
//   // add_filter('wpseo_title', 'franksullivan_products_archive_title');
//   // return "FOOBAR" . $title;
// }

// change page title
// franksullivan_products_archive_title();

?>

<section id="products" data-controller="Products">

  <header class="section--intro-header">

    <div class="container">

        <hgroup class="_column">

          <div class="columns">

            <hgroup class="section--intro-header--intro-blurb column is-12">
              <?= $products_intro_text ?>
            </hgroup>

          </div>

          <div class="columns is-hidden-mobile" data-aos="fade" data-aos-duration="1000" data-aos-delay="2000">

              <hgroup class="column is-6">
                <ul class="products--list-summary">
                <?php while ($posts->have_posts()) : $posts->the_post(); ?>
                  <li><a href="#product-<?php the_ID() ?>" data-scroll-to><?php the_title() ?></a></li>
                <?php endwhile; wp_reset_postdata() ?>
                </ul>
              </hgroup>
          </div>

      </hgroup>

    </div>
    <!-- /container -->

    <?php if ($products_header_image): ?>
    <figure class="section--intro-header--background-image _rellax" _data-rellax-speed="0.25">
      <?php echo get_responsive_image($products_header_image, 'largest', 'bg', false) ?>
    </figure>
    <?php endif; ?>

  </header>

  <div class="container is-hidden-desktop">

    <ul class="products--list-summary--mobile">
    <?php while ($posts->have_posts()) : $posts->the_post(); ?>
      <li><a href="#product-<?php the_ID() ?>" data-scroll-to><?php the_title() ?></a></li>
    <?php endwhile; wp_reset_postdata() ?>
    </ul>

  </div>

  <div class="products--list container">
    <div class="columns is-multiline">
      <?php while ($posts->have_posts()) : $posts->the_post(); ?>
        <?php get_template_part('templates/products/product-item') ?>
      <?php endwhile; ?>
    </div>
  </div>

  <footer class="products--suppliers container">
    <article data-accordion>
      <header>
        <h3 class="title is-5">
          <a href="#" class="accordion--item--toggle">
            <?php pll_e("Frank & Sullivan suppliers list") ?>
          </a>
          <a href="#" class="accordion--item--toggle pull-right"><i class="accordion--item--toggle--icon ion-ios-arrow-down arrow"></i></a>
        </h3>
      </header>
      <footer class="accordion--item--content">
        <?= $products_suppliers ?>
      </footer>
    </article>
  </footer>

  <?php get_template_part('templates/posts/recent-posts-square') ?>

</section>
